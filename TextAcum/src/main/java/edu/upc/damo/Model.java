package edu.upc.damo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Josep M on 08/10/2014.
 */
public class Model implements Iterable<CharSequence> {
    private List<CharSequence> dades;
    private OnCanviModelListener observador;

    public Model() {
        dades = new ArrayList<CharSequence>();
    }

    public void afegir(CharSequence s) {
        dades.add(s);
        avisaObservador();
    }

    /***
     * @param pos Posicio dins del model; base zero
     */
    public void remove(int pos) {
        if (pos >= 0 && pos < dades.size()) {
            dades.remove(pos);
            avisaObservador();
        }
    }

    public void setOnCanviModelListener(OnCanviModelListener observador) {
        this.observador = observador;
    }

    private void avisaObservador() {
        if (observador != null)
            observador.onNovesDades();
    }

    @Override
    public Iterator<CharSequence> iterator() {
        return dades.listIterator();
    }


    public interface OnCanviModelListener {
        public void onNovesDades();
    }
}
